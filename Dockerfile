# Deriving the latest base image
FROM python:3.10

# Labels as key value pair
LABEL Maintainer="huyphan"

# Sets the working directory in the container
WORKDIR /usr/app/src

# Copies the dependency files to the working directory
COPY requirements.txt ./

# Install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy environment folder to the working directory
COPY .env ./

# Copy python files to the working directory
COPY *.py ./

# Set entrypoint
ENTRYPOINT ["python3"]