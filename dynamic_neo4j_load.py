import pandas as pd
import numpy as np
import time

from sqlalchemy import create_engine
from utilities import Neo4jConnection
from os import environ, path
from dotenv import load_dotenv

# load .env - remember replace sample .env with your .env
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

# Set up database connection engine
POSTGRESQL_USERNAME = environ.get('POSTGRESQL_USERNAME')
POSTGRESQL_PASSWORD = environ.get('POSTGRESQL_PASSWORD')
POSTGRESQL_SERVER = environ.get('POSTGRESQL_SERVER')
POSTGRESQL_PORT = environ.get('POSTGRESQL_PORT')
POSTGRESQL_DB_NAME = environ.get('POSTGRESQL_DB_NAME')

NEO4J_USERNAME = environ.get('NEO4J_USERNAME')
NEO4J_PASSWORD = environ.get('NEO4J_PASSWORD')
NEO4J_SERVER = environ.get('NEO4J_SERVER')
NEO4J_PORT = environ.get('NEO4J_PORT')

# Define graph connection
GRAPH_CONN = Neo4jConnection(uri="bolt+ssc://{0}:{1}".format(NEO4J_SERVER, NEO4J_PORT), user=NEO4J_USERNAME, pwd=NEO4J_PASSWORD)

# Set up PostgreSQL connection engine
NEO4J_ENGINE = create_engine('postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(POSTGRESQL_USERNAME, POSTGRESQL_PASSWORD, POSTGRESQL_SERVER, POSTGRESQL_PORT, POSTGRESQL_DB_NAME))
NEO4J_CONN = NEO4J_ENGINE.connect()

# Empty Graph Database
GRAPH_CONN.query('MATCH (n) DETACH DELETE n')


class DataImport():

    def get_node_tables(self):
        df = pd.read_sql_query("""
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema='public'
                AND table_type='BASE TABLE' AND table_name LIKE 'NODE_%%'
            """.strip(), con=NEO4J_CONN)
        return df['table_name'].values.tolist()

    def get_brel_tables(self):
        df = pd.read_sql_query("""
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema='public'
                AND table_type='BASE TABLE' AND table_name LIKE 'BREL_%%'
            """.strip(), con=NEO4J_CONN)
        return df['table_name'].values.tolist()

    def get_srel_tables(self):
        df = pd.read_sql_query("""
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema='public'
                AND table_type='BASE TABLE' AND table_name LIKE 'SREL_%%'
            """.strip(), con=NEO4J_CONN)
        return df['table_name'].values.tolist()

    def insert_data(self, query, node_name1, node_name2, rows, batch_size=10000):
        # Function to handle the updating the Neo4j database in batch mode.
        total = 0
        batch = 0
        start = time.time()
        result = None

        while batch * batch_size < len(rows):
            res = GRAPH_CONN.query(query, parameters={'rows': rows[batch*batch_size:(batch+1)*batch_size].to_dict('records')})
            total += res[0]['total']
            batch += 1
            result = {
                "node_name1": node_name1,
                "node_name2": node_name2,
                "total": total,
                "batches": batch,
                "time": time.time()-start
            }
            print(result)
        return result

    def add_node(self, node_name, node_ids, data):
        start = time.time()
        properties = []
        node_ids_list = []
        if type(node_ids) == list:
            for node_id in node_ids:
                node_ids_list.append('{0}: row.{0}'.format(node_id))
        else:
            node_ids_list.append('{0}: row.{0}'.format(node_ids))
        main_query = 'UNWIND $rows AS row MERGE (n:{0}'.format(node_name) + ' {' + '{0}'.format(', '.join(node_ids_list)) + '}' + ')'
        sub_query = ' RETURN count(*) as total'
        if len(data.columns.values) == 1:
            query = main_query + sub_query
        else:
            cols = data.columns.values
            for i in np.delete(cols, np.where(np.in1d(cols, node_ids))):
                properties.append('n.{0} = row.{0}'.format(i))
            query = main_query + ' ON CREATE SET {0} ON MATCH SET {0}'.format(', '.join(properties)) + sub_query
        res = GRAPH_CONN.query(query, parameters={'rows': data.to_dict('records')})
        result = {"node_name": node_name, "total": res[0]['total'], "time": time.time()-start}
        print(result)
        return result

    def add_relationship(self, node_name1, rel_name, node_name2, data, batch_size=5000):
        node_ids_list = []
        properties = []
        for i in list(data.columns.values):
            if list(data.columns.values).index(i) < 2:
                node_ids_list.append('{0}: row.{0}'.format(i))
            else:
                properties.append('r.{0} = row.{0}'.format(i))
        main_query = 'UNWIND $rows AS row MATCH (a:{0}'.format(node_name1) + ' {' + node_ids_list[0] + '}), (' + 'b:{0}'.format(node_name2) + ' {' + node_ids_list[1] + '}) MERGE (a)-[r:' + rel_name + ']->(b)'
        property_query = ' ON CREATE SET {0} ON MATCH SET {0}'.format(', '.join(properties))
        sub_query = ' RETURN count(r) as total'
        if len(data.columns.values) == 2:
            query = main_query + sub_query
        else:
            query = main_query + property_query + sub_query
        # print(query)
        return self.insert_data(query, node_name1, node_name2, data, batch_size)

    def add_selfrelationship(self, node_name, rel_name, data, batch_size=5000):
        properties = ['{0}: row.{1}'.format(data.columns.values[1], data.columns.values[0]), '{0}: row.{0}'.format(data.columns.values[1])]
        query = 'UNWIND $rows AS row MATCH (a:{0}'.format(node_name) + ' {' + properties[0] + '}), (' + 'b:{0}'.format(node_name) + ' {' + properties[1] + '}) MERGE (a)-[r:' + rel_name + ']->(b) RETURN count(r) as total'
        # print(query)
        return self.insert_data(query, node_name, node_name, data, batch_size)

    def import_node(self):
        for table_name in self.get_node_tables():
            print(table_name)
            node_name = table_name.replace("NODE_", "")
            data = pd.read_sql_query('select * from "{0}"'.format(table_name), con=NEO4J_CONN)
            node_id = data.columns[0]
            GRAPH_CONN.query('CREATE CONSTRAINT {0}s IF NOT EXISTS ON (p:{1}) ASSERT p.{0} IS UNIQUE'.format(node_id, node_name))
            self.add_node(node_name, node_id, data)

    def import_brel(self):
        for table_name in self.get_brel_tables():
            print(table_name)
            node_name1 = table_name.replace("BREL_", "").split('_')[0]
            node_name2 = table_name.replace("BREL_", "").split('_')[1]
            data = pd.read_sql_query('select * from "{0}"'.format(table_name), con=NEO4J_CONN)
            rel_name = data['REL_NAME'].unique()[0]
            data.drop(columns=['REL_NAME'], inplace=True)
            self.add_relationship(node_name1, rel_name, node_name2, data)

    def import_srel(self):
        for table_name in self.get_srel_tables():
            print(table_name)
            node_name = table_name.replace("SREL_", "")
            data = pd.read_sql_query('select * from "{0}"'.format(table_name), con=NEO4J_CONN)
            rel_name = data['REL_NAME'].unique()[0]
            data.drop(columns=['REL_NAME'], inplace=True)
            self.add_selfrelationship(node_name, rel_name, data)

    def execute(self):
        # import node
        self.import_node()
        # import bi-direction relationship
        self.import_brel()
        # import self-relationship
        self.import_srel()


if __name__ == '__main__':
    DataImport().execute()

# Close the database connection
NEO4J_CONN.close()
NEO4J_ENGINE.dispose()
GRAPH_CONN.close()
