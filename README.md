# POSTGRESQL TO NEO4J GRAPH DATA PIPELINE
This repository contains data pipeline for extracting pre-defined tables in PostgreSQL and loading data into Neo4j Graph Database. You can design graph schema, node, relationships between nodes via designing and creating pre-defined tables in PostgreSQL. The data pipeline will load all the pre-defined tables in PostgreSQL automatically and import into Neo4j Graph Database. The data pipeline can be scheduled using Apache Airflow.

## I. Loading and Creating Nodes
In order to create nodes in Neo4j Graph Database, you need to create tables with following rules:
1. Create table name as ```NODE_NodeName```. Example: NODE_User, NODE_Phone, NODE_Email, etc. The text string, which is after **NODE_** keyword, will be used to create node name in the graph database.
2. The first column of node table will be used as primary key, and the rest of columns will be used as node properties.
3. You have to make sure that primary key for each node must be unique, not empty and correct data type.

Example for NODE_User
|user_id|full_name|address|
|:---|:----:|---:|
|1|Jimmy|123 Nguyen Hue, District 1, HCM|
|2|Jason|789 Hong Bang, District 5, HCM|
|2|Tan|658 Nguyen Trai, District 5, HCM|

In this example, the data pipeline will create **User** node with **user_id** as primary key and other properties are **full_name** and **address**.

## II. Loading and Creating Relationships Between 2 Different Nodes
In order to create relationships between 2 different nodes in Neo4j Graph Database, you need to create tables with following rules:
1. Create table name as ```BREL_NodeName1_NodeName2```. Example: BREL_User_Phone, BREL_User_Email, etc. ```NodeName1``` is the beginning node and ```NodeName2``` is the ending node. You need to specify these in the table name as syntax above.
2. The first column of relationship table is the primary key of ```NodeName1```.
3. The second column of relationship table is used to define relationship label. This column name must be named as **REL_NAME**. And you have to specify this column's value as relationship label name.
4. The third column of relationship table is the primary key of ```NodeName2```.
5. The combination of first and third column must be not empty and correct data type.
6. The rest of the columns from fourth column onward will be used as relationship properties.

Example: BREL_User_Phone
|user_id|REL_NAME|phone|property_1|property_2|
|:---|:----:|:----:|:----:|---:|
|1|hasValue|0972654963|property_1_value|property_2_value|
|2|hasValue|0658486578|property_1_value|property_2_value|
|5|hasValue|3659452698|property_1_value|property_2_value|

In this example, the data pipeline will create relationships between **User** node and **Phone** node using mapping key (primary key) as **user_id** and **phone**. The relationship label will be named using the second column's value is *hasValue*. The rest of the columns from fourth column onward will be used as relationship properties.

## III. Loading and Creating Self Relationships for a Node
A node having self-relationship is a node, which has relationship with itself. In order to create self-relationships for a node in Neo4j Graph Database, you need to create tables with following rules:
1. Create table name as ```SREL_NodeName```. Example: SREL_Employee, etc
2. The first column of relationship table is the primary key of ```NodeName```. And you must rename this column's name with prefix **pre_**.
3. The second column of relationship table is used to define relationship label. This column name must be named as **REL_NAME**. And you have to specify this column's value as relationship label name.
4. The third column of relationship table is the primary key of ```NodeName```. You do not need to rename this column's name.
5. The combination of first and third column must be not empty and correct data type.
6. At the moment, you cannot add properties for this kind of relationship.

Example: SREL_Employee
|pre_employee_id|REL_NAME|employee_id|
|:---|:----:|---:|
|1|reportTo|4|
|2|reportTo|5|
|3|reportTo|5|

In this example, the data pipeline will create self-relationship for **Employee** node using primary key as **employee_id**. The relationship label will be named using the second column's value is *reportTo*.

## IV. Deployment
#### 1. Build data pipeline image and Push to Gitlab Container Registry
```
## Login to Gitlab using personal account token
docker login registry.gitlab.com
## Build image
docker build -t registry.gitlab.com/username/postgresql-to-neo4j-graph .
## Push image to Gitlab
docker push registry.gitlab.com/username/postgresql-to-neo4j-graph
```

#### 2. Run data pipeline
```
docker run image_name dynamic_neo4j_load.py
```
